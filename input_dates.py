from datetime import datetime, timedelta


def input_dates() -> tuple[datetime]:
    """ Get starting and ending date values from command line input
    :returns: starting and ending dates for scrape
    :rtype: datetime
    """

    start = None
    end = None

    while start is None:
        starting_date_response = get_start_date()

        if starting_date_response == '':
            start = get_todays_date()
        else:
            try:
                start = datetime.strptime(starting_date_response, '%m%d%Y')
            except ValueError:
                print("Invalid start date")

    print(f"starting date chosen as {start}")

    while end is None:
        ending_date_response = get_end_date()

        if ending_date_response == '':
            end = start - timedelta(days=1)
        else:
            try:
                end_date = datetime.strptime(ending_date_response, '%m%d%Y')
                if end_date > start:
                    print("Invalid time range (ending date must be earlier than start date)")
                else:
                    end = end_date
            except ValueError:
                print("Invalid end date format")

    print(f"ending date chosen as {end}")

    return start, end


def get_start_date() -> str:
    return input("Enter a starting date in the format of MMddYYYY, or press enter for today")


def get_end_date() -> str:
    return input("Enter an ending date (not included) in the format of MMddYYYY or "
                 "press enter to process one day only")


def get_todays_date() -> datetime:
    return datetime.today()