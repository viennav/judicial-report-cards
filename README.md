# judicial-reportcards

Repository for the judicial scorecard project with the Nashville NAACP

Dependencies:
* Python 3.9
* Pipenv

Quick start:
* Run `pipenv install` to create a virtual environment and install all project-specific dependencies

* To collect a range of HTML pages of defendant details for individuals appearing in Metro Nashville Criminal Court,
run `pipenv run python scrape_html_pages.py`
* To write all `.html` files to a single `all_results.csv` file, run `pipenv run python merge_html`
