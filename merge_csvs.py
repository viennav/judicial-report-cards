import pandas as pd
import pathlib

""" Defines a method that takes all csv files in the output directory and merge into a deduplicated csv
    file called 'merged.csv'. 
    
    This acts on any existing 'merge' file as well, so that
    file is cumulative.
"""


def merge_csvs():
    """ Merge all csv files in output directory"""

    output_dir = pathlib.Path("output")
    files = output_dir.glob("*.csv")

    dataframes = []

    for file in files:
        df = pd.read_csv(file)
        dataframes.append(df)

    merged = pd.concat(dataframes).drop_duplicates()

    with open("output/merged.csv", "w") as outfile:
        merged.to_csv(outfile, index=False)


if __name__ == "__main__":
    merge_csvs()
