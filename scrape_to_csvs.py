import csv
from datetime import datetime, timedelta

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException, ElementNotInteractableException, \
    ElementClickInterceptedException
from webdriver_manager.chrome import ChromeDriverManager
from constants import FIELDS_WITH_XPATHS
from input_dates import input_dates

""" This method is outdated / unreliable compared to scrape_html_pages"""


def scrape(starting_date: datetime, ending_date: datetime) -> None:
    file = f"output/results-{datetime.now()}.csv"
    with open(file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(FIELDS_WITH_XPATHS.keys())

    site = "https://sci.ccc.nashville.gov/Reporting/TrialCourtScheduledAppearance"
    options = Options()
    options.headless = True
    options.page_load_strategy = 'eager'
    browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)

    browser.get(site)

    # scrape_range is a timedelta instance with a 'days' attribute used as the range incrementer
    scrape_range = starting_date - ending_date

    date_input = WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.ID, "reportDate")))
    date_input.send_keys(starting_date.strftime('%m/%d/%Y'))
    search_button = browser.find_element_by_id('report-button')
    search_button.click()

    current_scrape_date = starting_date
    for day in range(scrape_range.days):
        print(f"scraping {current_scrape_date}")

        defendant_links_list = browser.find_elements_by_xpath("//*[@data-content='View Defendant Details']")

        identified_defendant_pages = []

        for i in range(len(defendant_links_list)):

            # Click on each defendant name as link from starting page
            links = browser.find_elements_by_xpath("//*[@data-content='View Defendant Details']")
            link = WebDriverWait(browser, 30).until(EC.element_to_be_clickable((By.LINK_TEXT, links[i].text)))

            try:
                link.click()
            except (ElementNotInteractableException, ElementClickInterceptedException) as ex:
                print(f"skipping entry {i} on this date's page due to {ex.msg}")
                browser.save_screenshot(f"def-{i}-unclickable-details.png")
                continue

            # Locate and click on the Detailed Criminal History button (opens new window)
            try:
                criminal_history_link = WebDriverWait(browser, 120).until(
                    EC.presence_of_element_located((By.CLASS_NAME, "detailed-criminal-history-link")))
            except TimeoutException:
                print(f"timed out trying to find link for Detailed Criminal History")
                browser.save_screenshot()
                browser.back()
                continue

            criminal_history_link.click()

            defendant_page_url = browser.current_url

            # Switch focus to Detailed Criminal History tab
            tabs = browser.window_handles
            browser.switch_to.window(tabs[1])

            # Wait for page load as defined by appearance of parent content element
            try:
                WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.CLASS_NAME, 'crim-history-wrapper')))
            except TimeoutException:
                print(f"detailed history failed to load for {defendant_page_url}")
                browser.close()
                browser.switch_to.window(tabs[0])
                browser.back()
                continue

            case_sections = browser.find_elements_by_class_name("crim-history-row")

            if defendant_page_url not in identified_defendant_pages:
                print(defendant_page_url)
                case_section_rows = []

                for section in case_sections:
                    row = []

                    for xpath in FIELDS_WITH_XPATHS.values():
                        try:
                            field_value = section.find_element_by_xpath(xpath).text
                        except NoSuchElementException:
                            field_value = ''
                        row.append(field_value)

                    case_section_rows.append(row)

                with open(file, 'a', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    for row in case_section_rows:
                        writer.writerow(row)

                identified_defendant_pages.append(defendant_page_url)

            browser.close()
            browser.switch_to.window(tabs[0])
            browser.back()

        current_scrape_date = current_scrape_date - timedelta(days=1)
        current_date_string = current_scrape_date.strftime('%m/%d/%Y')

        date_input = browser.find_element_by_id('reportDate')
        date_input.send_keys(current_date_string)

        search_button = browser.find_element_by_id('report-button')
        search_button.click()

    browser.quit()


# Execution
if __name__ == '__main__':
    start, end = input_dates()
    scrape(start, end)
